﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldOfWarShips
{
    class Program
    {
        enum Cell { Alive, Died, Miss, Empty }
        static void Main(string[] args)
        {
            #region Переменные
            const int N = 10;
            Cell[,] UserField = new Cell[N, N];
            Cell[,] CompField = new Cell[N, N];
            int userShips = N, compShips = N;

            int step = 0;

            int ii, jj;
            Random rnd = new Random();

            bool play = true;
            #endregion

            #region Обнуление полей
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    UserField[i, j] = Cell.Empty;
                    CompField[i, j] = Cell.Empty;
                }
            }
            #endregion

            #region Расстановка кораблей 
            for (int k = 0; k < userShips; k++)
            {
                do
                {
                    ii = rnd.Next(0, N);
                    jj = rnd.Next(0, N);
                }
                while (UserField[ii, jj] != Cell.Empty);

                UserField[ii, jj] = Cell.Alive;
            }

            for (int k = 0; k < compShips; k++)
            {
                do
                {
                    ii = rnd.Next(0, N);
                    jj = rnd.Next(0, N);
                }
                while (CompField[ii, jj] != Cell.Empty);

                CompField[ii, jj] = Cell.Alive;
            }
            #endregion

            #region Игровой цикл
            while (play == true)
            {
                #region Очистить экран и вывести поля 
                Console.Clear();

                Console.WriteLine("Поле игрока");
                for (int i = 0; i < N; i++)
                {
                    for (int j = 0; j < N; j++)
                    {
                        switch (UserField[i, j])
                        {
                            case Cell.Alive:
                                Console.Write("K");
                                break;
                            case Cell.Died:
                                Console.Write("X");
                                break;
                            case Cell.Miss:
                                Console.Write("O");
                                break;
                            case Cell.Empty:
                                Console.Write(".");
                                break;
                        }
                    }
                    Console.WriteLine();
                }
                Console.WriteLine("------------------------");

                Console.WriteLine("Поле Компьютера");
                for (int i = 0; i < N; i++)
                {
                    for (int j = 0; j < N; j++)
                    {
                        switch (CompField[i, j])
                        {
                            case Cell.Alive:
                                Console.Write(".");
                                break;
                            case Cell.Died:
                                Console.Write("X");
                                break;
                            case Cell.Miss:
                                Console.Write("O");
                                break;
                            case Cell.Empty:
                                Console.Write(".");
                                break;
                        }
                    }
                    Console.WriteLine();
                }
                #endregion

                #region Определение кто ходит и выстрел
                step++;
                if (step % 2 == 1)
                {
                    Console.WriteLine("Выстрел игрока");
                    do
                    {
                        Console.Write("Введите строку: ");
                        ii = int.Parse(Console.ReadLine()) - 1;

                        Console.Write("Введите столбец: ");
                        jj = int.Parse(Console.ReadLine()) - 1;
                    }
                    while (ii < 0 || jj < 0 || ii > N - 1 || jj > N - 1 ||
                    CompField[ii, jj] == Cell.Miss ||
                    CompField[ii, jj] == Cell.Died);

                    if (CompField[ii, jj] == Cell.Empty)
                    {
                        CompField[ii, jj] = Cell.Miss;
                    }
                    else if (CompField[ii, jj] == Cell.Alive)
                    {
                        CompField[ii, jj] = Cell.Died;
                        compShips--;
                    }
                }
                else
                {
                    Console.WriteLine("Выстрел компа");
                    Console.WriteLine("Нажмите Enter");
                    Console.ReadKey();
                    do
                    {
                        ii = rnd.Next(0, N);
                        jj = rnd.Next(0, N);
                    }
                    while (UserField[ii, jj] == Cell.Miss ||
                    UserField[ii, jj] == Cell.Died);

                    if (UserField[ii, jj] == Cell.Empty)
                    {
                        UserField[ii, jj] = Cell.Miss;
                    }
                    else if (UserField[ii, jj] == Cell.Alive)
                    {
                        UserField[ii, jj] = Cell.Died;
                        userShips--;
                    }
                }
                #endregion

                #region Все ли корабли убиты
                if (userShips == 0 || compShips == 0)
                {
                    play = false;
                }
                #endregion
            }
            #endregion

            #region Очистить экран и вывести поля 
            Console.Clear();

            Console.WriteLine("Поле игрока");
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    switch (UserField[i, j])
                    {
                        case Cell.Alive:
                            Console.Write("K");
                            break;
                        case Cell.Died:
                            Console.Write("X");
                            break;
                        case Cell.Miss:
                            Console.Write("O");
                            break;
                        case Cell.Empty:
                            Console.Write(".");
                            break;
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine("------------------------");

            Console.WriteLine("Поле Компьютера");
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    switch (CompField[i, j])
                    {
                        case Cell.Alive:
                            Console.Write(".");
                            break;
                        case Cell.Died:
                            Console.Write("X");
                            break;
                        case Cell.Miss:
                            Console.Write("O");
                            break;
                        case Cell.Empty:
                            Console.Write(".");
                            break;
                    }
                }
                Console.WriteLine();
            }
            #endregion

            #region Победитель
            if(userShips == 0)
            {
                Console.WriteLine("Компьютер победил");
            }
            else if (compShips == 0)
            {
                Console.WriteLine("Игрок победил");
            }
            #endregion

            #region Пауза
            Console.ReadKey();
            #endregion
        }
    }
}
