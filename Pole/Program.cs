﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pole
{
    class Program
    {
        static void Main(string[] args)
        {
            const int N = 7;
            int[,] mas = new int[N, N];

            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    //if(i>=j)
                    //if (j%2==0)
                    //if (i%2==0)
                    //if (i % 2 == 0 && j % 2 == 0 || i % 2 == 1 && j % 2 == 1)
                    //if (i+j == 6)
                    //if (i + j <= 6)
                    //if (i + j >= 6)
                    //if (i + j >= 7 && i < j || i + j <= 5 && i > j)
                    //if (i + j <= 6 && i <= j || i + j >= 6 && i >= j)
                    //if (i >= 0 && j == 3 || i == 3 && j >=0)
                    if (i==j || i+j == 6)
                    {
                        mas[i, j] = 1;
                    }
                    else
                    {
                        mas[i, j] = 0;
                    }
                }
            }

            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    Console.Write(mas[i, j]);
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
